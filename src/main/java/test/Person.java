package test;

public class Person {

    String name;

    public Person(String name) {
        this.name = name;
    }

    String upperCase() {
        return name.toUpperCase();
    }
}
