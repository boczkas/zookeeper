package test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTest {

    @Test
    public void upperCase_nameSmallLetters_upperCased() {
        Person person = new Person("jon");

        String actual = person.upperCase();

        assertEquals("JON", actual);
    }
}
